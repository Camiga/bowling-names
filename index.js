// Place your string here, and length to shorten it to.
const input = 'Hello world!';
const maxChars = 7;

// Function to reverse text.
function Reverse(text) {
  return text
    .split('')
    .reverse()
    .join('');
}

// Set the current output to be the new reversed string.
let output = Reverse(input);

// Script will run as long as this variable is true. This will be changed to false to stop the script.
let doWork = true;

// These checkpoints exist so that else statements don't fire when they shouldn't.
let checkPoint = true;
let checkPoint2 = false;
let checkPoint3 = false;
let checkPoint4 = false;

// These variables detail what the script is currently allowed to do.
let removeSpaces = true;
let removePunct = false;
let removeVowels = false;
let extremeSlice = false;

// Array that will store the process of shortening the sentence.
let process = [];

// Variable for storing how much data has been cut to determine a confidence score.
let removed = 0;

// While loop is used to remove parts of the string one by one.
while (doWork) {
  // Remove spaces first, if the string is too long, and there are any spaces to remove.
  if (
    removeSpaces &&
    output.length > maxChars &&
    Boolean(output.match(/\s/g))
  ) {
    // Replace whitespace with nothing.
    output = output.replace(/\s/, '');
    // Lower the confidence score slighly.
    removed += 0.2;
    // If the length is now suitable and we are up to here, end the script here.
  } else if (checkPoint && output.length <= maxChars) {
    // Stop the loop.
    doWork = false;
    process.push(Reverse(output));
    process.push('> Done at removing spaces.');
    // If the script is still up to here, but there are no more spaces to remove.
  } else if (checkPoint && !Boolean(output.match(/\s/g))) {
    // Disable this part.
    removeSpaces = false;
    // Enable the next part.
    removePunct = true;
    // Cross this checkpoint, so that the else statements don't run out of turn..
    checkPoint = false;
    // Prime the next checkpoint, so that their else statements can run.
    checkPoint2 = true;
    process.push(Reverse(output));
    process.push('> Spaces removed.');
  }

  // If we are now allowed to remove punctuation, the string is too long, and there is punctuation to remove.
  if (
    removePunct &&
    output.length > maxChars &&
    Boolean(output.match(/[^\w\s]|_/g))
  ) {
    // Remove anything that isn't whitespace, or any words.
    output = output.replace(/[^\w\s]|_/, '');
    // Lower the confidence score a bit more.
    removed += 0.4;
    // End the script if the length is correct.
  } else if (checkPoint2 && output.length <= maxChars) {
    doWork = false;
    process.push(Reverse(output));
    process.push('> Done at removing punctuation.');
  } else if (checkPoint2 && !Boolean(output.match(/[^\w\s]|_/g))) {
    // Disable paramaters for this piece of code, and enable the next ones.
    removePunct = false;
    removeVowels = true;
    checkPoint2 = false;
    checkPoint3 = true;
    process.push(Reverse(output));
    process.push('> Punctuation removed.');
  }

  // Remove vowels if needed.
  if (
    removeVowels &&
    output.length > maxChars &&
    Boolean(output.match(/[AEIOUaeiou]/g))
  ) {
    output = output.replace(/[AEIOUaeiou]/, '');
    process.push(Reverse(output));
    // Lower the confidence score a fair bit.
    removed += 0.8;
  } else if (checkPoint3 && output.length <= maxChars) {
    doWork = false;
    process.push('> Done at removing vowels.');
  } else if (checkPoint3 && !Boolean(output.match(/[AEIOUaeiou]/))) {
    removeVowels = false;
    extremeSlice = true;
    checkPoint3 = false;
    checkPoint4 = true;
    process.push('> Vowels removed.');
  }

  // If the string is still too long, crop the end of it.
  if (extremeSlice && output.length > maxChars) {
    // Remove one character from the beginning (end) of the string.
    output = output.substr(1);
    // Lower the confidence score a lot.
    removed += 1.6;
    // Since this piece of code is guranteed to get the string down to its max length, wait for it to happen.
  } else if (checkPoint4 && output.length <= maxChars) {
    extremeSlice = false;
    checkPoint4 = false;
    process.push(Reverse(output));
    process.push('> Done at brutal trimming.');
    // Stop the script.
    doWork = false;
  }
}

// Your final output will be given after the while loop has finished.
console.log('Final output:\n' + Reverse(output));

// (Optional) Program confidence in the string being recognisable.
console.log('Confidence: ' + Math.round(100 - (removed / maxChars) * 100));

// (Optional) List out the process of shortening the text.
console.log('Process:\n' + process.join('\n'));
